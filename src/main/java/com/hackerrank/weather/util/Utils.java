package com.hackerrank.weather.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hackerrank.weather.entity.Temperature;
import com.hackerrank.weather.entity.WeatherEntity;
import com.hackerrank.weather.model.Weather;
import com.hackerrank.weather.model.WeatherDTO;

public class Utils {
	
	public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	public static WeatherEntity convertToWeather(WeatherDTO weatherModel) {
		List<Temperature> tempListEntity = new ArrayList<>();
		WeatherEntity w = new WeatherEntity();
		w.setCity(weatherModel.getCity());
		String fecha = weatherModel.getDateString();
		w.setDate(stringToDate(fecha));
		w.setLat(weatherModel.getLat());
		w.setLon(weatherModel.getLon());
		w.setState(weatherModel.getState());
		
		List<Double> temperatureList = weatherModel.getTemperatures();
		
		for(Double tmp:temperatureList) {
			Temperature tmpObject = new Temperature();
			tmpObject.setWeather(w);
			tmpObject.setTemperature(tmp);
			tempListEntity.add(tmpObject);
		}
		
		w.setTemperatures(tempListEntity);
		return w;
	}
	
	public static WeatherEntity convertToWeather(Weather weatherModel) {
		List<Temperature> tempListEntity = new ArrayList<>();
		WeatherEntity w = new WeatherEntity();
		w.setCity(weatherModel.getCity());
		w.setDate(weatherModel.getDate());
		w.setLat(weatherModel.getLat());
		w.setLon(weatherModel.getLon());
		w.setState(weatherModel.getState());
		
		List<Double> temperatureList = weatherModel.getTemperatures();
		
		for(Double tmp:temperatureList) {
			Temperature tmpObject = new Temperature();
			tmpObject.setWeather(w);
			tmpObject.setTemperature(tmp);
			tempListEntity.add(tmpObject);
		}
		
		w.setTemperatures(tempListEntity);
		return w;
	}
	
	public static Weather convertToWeatherModel(WeatherEntity weatherEntity) {
		List<Double> tmpList = new ArrayList<>();
		Weather wModel = new Weather();
		wModel.setId(weatherEntity.getId());
		wModel.setCity(weatherEntity.getCity());
		wModel.setDate(weatherEntity.getDate());
		wModel.setLat(weatherEntity.getLat());
		wModel.setLon(weatherEntity.getLon());
		wModel.setState(weatherEntity.getState());
		
		for(Temperature tmpObject: weatherEntity.getTemperatures()) {
			tmpList.add(tmpObject.getTemperature());
		}
		
		wModel.setTemperatures(tmpList);
		
		return wModel;
	}
	
	public static List<Weather> convertToWeatherModelList(List<WeatherEntity> weatherEntityList){
		List<Weather> response = new ArrayList<>();
		
		for(WeatherEntity weatherEntity:weatherEntityList) {
			response.add(convertToWeatherModel(weatherEntity));
		}
		
		return response;
	}
	
	public static Date stringToDate(String date) {
		 Date newDate = null;
		 try {
			 newDate = sdf.parse(date);
		 }catch(Exception e) {
			 
		 }
		 return newDate;
	 }
	 
	 public static String dateToString(Date date) {
		 return sdf.format(date);
	 }

}
