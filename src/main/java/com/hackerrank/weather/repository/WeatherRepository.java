package com.hackerrank.weather.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hackerrank.weather.entity.WeatherEntity;

@Repository("weatherRepository")
public interface WeatherRepository extends JpaRepository<WeatherEntity, Integer> {
	public Optional<WeatherEntity> findById(Integer id);
	
	@Query("select w from WeatherEntity w where UPPER(w.city) in ?1 order by w.id asc")
	public List<WeatherEntity> findByCity(List<String> city);
	
	@Query("select w from WeatherEntity w where PARSEDATETIME(w.date,'yyyy-MM-dd') = ?1 order by w.id asc")
	public List<WeatherEntity> findByDate(String date);
}
