package com.hackerrank.weather.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hackerrank.weather.entity.Temperature;

@Repository("temperatureRepository")
public interface TemperatureRepository extends JpaRepository<Temperature, Integer>{

}
