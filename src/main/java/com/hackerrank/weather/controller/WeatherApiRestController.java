package com.hackerrank.weather.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hackerrank.weather.entity.WeatherEntity;
import com.hackerrank.weather.model.Weather;
import com.hackerrank.weather.model.WeatherDTO;
import com.hackerrank.weather.service.WeatherService;
import com.hackerrank.weather.util.Utils;


@Controller
@RequestMapping("weather")
public class WeatherApiRestController {
	
	@Autowired
	WeatherService weatherService;
	
	@PostMapping()
	public ResponseEntity<?> createUser(@RequestBody WeatherDTO weatherModel){
		
		WeatherEntity weatherObject = Utils.convertToWeather(weatherModel);
		WeatherEntity weather = weatherService.create(weatherObject);
		
		Weather weatherModelResponse = Utils.convertToWeatherModel(weather);
		
		return new ResponseEntity<>(weatherModelResponse, HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getMetaData(@PathVariable("id") Integer id) {
		WeatherEntity resp = weatherService.findById(id);
		if(resp != null) {
			Weather weatherModelResponse = Utils.convertToWeatherModel(resp);
			return new ResponseEntity<Weather>(weatherModelResponse, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(resp, HttpStatus.NOT_FOUND);
		}
		
	}
	

	@GetMapping()
	public ResponseEntity<?> getMetaDataByParams(@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "sort", required = false) String sort,
			@RequestParam(value = "date", required = false) String date) {
		if(city != null) {
			List<WeatherEntity> resp = weatherService.findByCity(city);
			if (resp != null && !resp.isEmpty()) {
				List<Weather> weatherModelResponse = Utils.convertToWeatherModelList(resp);
				setSort(weatherModelResponse, sort);
				return new ResponseEntity<>(weatherModelResponse, HttpStatus.OK);
			} else {
				return new ResponseEntity<List<WeatherEntity>>(resp, HttpStatus.NOT_FOUND);
			}

		}
		
		if(date != null) {
			List<WeatherEntity> resp = weatherService.findByDate(date);
			if (resp != null && !resp.isEmpty()) {
				List<Weather> weatherModelResponse = Utils.convertToWeatherModelList(resp);
				setSort(weatherModelResponse, sort);	
				return new ResponseEntity<>(weatherModelResponse, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(resp, HttpStatus.NOT_FOUND);
			}

		}
		
		List<WeatherEntity> all = weatherService.findAll();
		List<Weather> weatherModelResponseAll = Utils.convertToWeatherModelList(all);	
		setSort(weatherModelResponseAll, sort);		
        return new ResponseEntity<>(weatherModelResponseAll, HttpStatus.OK);
				
	}
	
	private void setSort(List<Weather> weatherModelResponse, String sort) {
		if(sort != null && sort.equalsIgnoreCase("date")) {
			weatherModelResponse.sort((d1, d2) -> d1.getDate().compareTo(d2.getDate()));
		}
        if(sort != null && sort.equalsIgnoreCase("-date")) {
        	weatherModelResponse.sort((d1, d2) -> d2.getDate().compareTo(d1.getDate()));
		}
	}
	
}
