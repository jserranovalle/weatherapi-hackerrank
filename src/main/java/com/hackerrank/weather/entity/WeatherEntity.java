package com.hackerrank.weather.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class WeatherEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
    private Date date;

    private Float lat;
    private Float lon;
    private String city;
    private String state;
    
    @OneToMany(mappedBy = "weather")
    private List<Temperature> temperatures;
}
