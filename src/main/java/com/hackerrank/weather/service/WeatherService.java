package com.hackerrank.weather.service;

import java.util.List;

import com.hackerrank.weather.entity.WeatherEntity;

public interface WeatherService {
	public abstract WeatherEntity create(WeatherEntity weather);
	public abstract WeatherEntity findById(Integer id);
	public abstract List<WeatherEntity> findAll();
	public abstract List<WeatherEntity> findByCity(String city);
	public abstract List<WeatherEntity> findByDate(String date);
}
