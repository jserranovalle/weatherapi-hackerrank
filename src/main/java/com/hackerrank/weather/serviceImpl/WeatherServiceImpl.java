package com.hackerrank.weather.serviceImpl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackerrank.weather.entity.Temperature;
import com.hackerrank.weather.entity.WeatherEntity;
import com.hackerrank.weather.repository.TemperatureRepository;
import com.hackerrank.weather.repository.WeatherRepository;
import com.hackerrank.weather.service.WeatherService;

@Service("weatherService")
public class WeatherServiceImpl implements WeatherService{

	@Autowired
	private WeatherRepository weatherRepository;
	
	@Autowired 
	private TemperatureRepository temperatureRepository;
	
	@Override
	public WeatherEntity create(WeatherEntity weather) {
		WeatherEntity weathersaved = weatherRepository.save(weather);
		List<Temperature> tpmList = weather.getTemperatures();
		temperatureRepository.saveAll(tpmList);
		return weatherRepository.save(weathersaved);
	}

	@Override
	public WeatherEntity findById(Integer id) {
		Optional<WeatherEntity> dat = weatherRepository.findById(id);
		if(dat.isEmpty()) {
			return null;
		}
		return dat.get();
	}

	@Override
	public List<WeatherEntity> findAll() {
		return weatherRepository.findAll();
	}

	@Override
	public List<WeatherEntity> findByCity(String city) {
		List<String> cities = new ArrayList<>();
		String[] cit = city.split(",");
		for(String c:cit) {
			cities.add(c.toUpperCase());
		}		
		return weatherRepository.findByCity(cities);
	}

	@Override
	public List<WeatherEntity> findByDate(String date) {
		List<WeatherEntity> weatherListResponse = new ArrayList<>();
		List<WeatherEntity> weatherList = findAll();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			Date newDate = sdf.parse(date);
			//date = sdf.format(newDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(newDate);
			date = sdf.format(newDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		for(WeatherEntity weather:weatherList) {
			Date d =weather.getDate();
			String dateStr = sdf.format(d);
			if(dateStr.equalsIgnoreCase(date)) {
				weatherListResponse.add(weather);
			}
		}
		return weatherListResponse;
	}

}
